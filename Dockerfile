FROM azul/zulu-openjdk-alpine:17

ARG MAVEN_VERSION=3.9.0
ARG MAVEN_DOWNLOAD=https://dlcdn.apache.org/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz
ARG MAVEN_INSTALL_DIR=/opt

RUN set -x \
  && apk add --no-cache bash curl tar \
  && curl -fsSLO --compressed ${MAVEN_DOWNLOAD} \
  && mkdir -p ${MAVEN_INSTALL_DIR} \
  && ls -lart \
  && tar -xf apache-maven-${MAVEN_VERSION}-bin.tar.gz -C ${MAVEN_INSTALL_DIR} --verbose \
  && ls -lart ${MAVEN_INSTALL_DIR} \
  && ln -s ${MAVEN_INSTALL_DIR}/apache-maven-${MAVEN_VERSION}/bin/mvn /usr/bin/mvn \
  && ls -lart /usr/bin | grep mvn \
  && mvn --version

COPY . .

RUN echo "*** WELCOME SIMPLE JAVA CONTAINER ***"